export function draw(canvas, socket, roomId) {
  var ctx = canvas.getContext("2d");
  fitToParent(canvas);

  var admin = false;

  socket.on("admin", function(is) {
    admin = is;
  });

  var brushSize = 1;
  var brushColor = "orange";
  var dragging = false;

  socket.on("point", function(point) {
    point.x = point.x * canvas.width;
    point.y = point.y * canvas.height;
    ctx.lineWidth = brushSize * 2;
    ctx.lineTo(point.x, point.y);
    ctx.strokeStyle = brushColor;
    ctx.stroke();
    ctx.beginPath();
    ctx.arc(point.x, point.y, brushSize, 0, 2 * Math.PI, false);
    ctx.fillStyle = brushColor;
    ctx.fill();
    ctx.beginPath();
    ctx.moveTo(point.x, point.y);
  });

  socket.on("mouseup", function() {
    dragging = false;
    ctx.beginPath();
  });

  canvas.addEventListener("mousemove", function(e) {
    if (admin && dragging) {
      var rect = canvas.getBoundingClientRect();
      var xcor = (e.clientX - rect.left) / canvas.width;
      var ycor = (e.clientY - rect.top) / canvas.height;

      socket.emit("point",
        {
          x: xcor,
          y: ycor,
          roomId: roomId
      });
    }
  });

  canvas.addEventListener("mousedown", function() {
    if (admin) 
      dragging = true;
    
  });

  canvas.addEventListener("mouseup", function() {
    if (admin) {
      dragging = false;
      ctx.beginPath();
      socket.emit("mouseup", { roomId: roomId });
    }
  });

  document.body.addEventListener("mouseup", function() {
    if (admin) {
      dragging = false;
      ctx.beginPath();
      socket.emit("mouseup", { roomId: roomId });
    }
  });

  function fitToParent(canvas) {
    canvas.style.width = "100%";
    canvas.width = canvas.offsetWidth;
    canvas.height = canvas.offsetWidth / 2;
  }

  // Color palette 
  var palette = document.getElementById("color-palette");
  var colors = [
    "orange", "#ff2f2f", "#029be4",
    "#1db568", "#292929"
  ];

  for (var i = 0; i < colors.length; i++) {
    var color = document.createElement("td"); 
    color.style.backgroundColor = colors[i]; 
    color.value = colors[i]; 
    color.addEventListener("click", changeBrushColor); 
    palette.appendChild(color);
  }

  function changeBrushColor(e) {
    if (admin)
      socket.emit("brush-color", {
        roomId: roomId, 
        color: e.target.value
      });
  }

  socket.on("color-changed", function(color) {
    brushColor = color;
  });

  // Brush size
  var selectBrushSize = document.getElementById("brush-size"); 
  selectBrushSize.addEventListener("change", changeBrushSize); 

  function changeBrushSize(e) {
    socket.emit("brush-size", {
      roomId: roomId, 
      size: e.target.value
    });
  }

  socket.on("size-changed", function(size) {
    brushSize = size;
  });
}
