import React from "react";

export const About = (props) => {
  return(
    <div className="container">
      <div className="col-lg-7">
        <h3>About</h3>
        <p>
          Single page React web application, built on top of the Node.js and Express.
        </p>
        <p>
          Application is still under development and you may notice
          some bugs. Although, if you do notice and you're willing to help me 
          by informing me about it, go ahead, I'll be glad.
          Unfortunately, for hosting I wasn't able to afford anything more than 
          Heroku's free dynos.
        </p>
        <p>
          For any other info contact me via e-mail. More info on: www.serjuice.online.
        </p>
        <p>Thank you so much, source code is available on Github. &hearts;</p>
      </div>
      <div className="col-lg-5">
        <h3>Why?</h3>
        <ul className="list-group">
          <li className="list-group-item">
            No additional software
          </li>
          <li className="list-group-item">
            No sign in/up required  
          </li>
          <li className="list-group-item">
            Multi-platform
          </li>
           <li className="list-group-item">
            Simplicity and usability
          </li>
        </ul>
      </div>
    </div>
  );
}
