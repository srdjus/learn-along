import React from "react";
import {Link} from "react-router";

export const Header = (props) => {
  return(
    <nav className="navbar navbar-inverse bg-inverse navbar-static-top">
      <div className="container">
        <div className="navbar-header">
          <ul className="nav navbar-nav">
            <li><Link to={"/create"}>Create Room</Link></li>
            <li><Link to={"/about"}>About</Link></li>
          </ul>
        </div>
      </div>
    </nav>
  );
}
