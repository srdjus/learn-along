import React from "react";
import {Header} from "Header";
import {Error} from "Error";

// WebSocket for communcation with server
import io from "socket.io-client";
let socket = io();

export class Root extends React.Component {
  constructor() {
    super();
    this.state = {
      status: 200
    }
  }

  componentDidMount() {
    // Not a real "http status code", just my personal imitation
    // of letting people know what is happening 
    socket.on("status", s => {
      this.setState({
        status: s
      });
    });
  }

  render() {
    let Content = "";
    if (this.state.status === 200) {
      let children = React.Children.map(this.props.children,
        (child) => React.cloneElement(child, {
        socket: socket
      }));
      
      Content = (
        <div className="col-xs-10 col-xs-offset-1">
          {children}
        </div>
      );
    }

    else if (this.state.status != 200) {
      Content = (
        <Error status={this.state.status} />
      );
    }

    return (
      <div className="container-fluid">
        <div className="row">
          <Header />
        </div>
        <div className="row">
          {Content}
        </div>
      </div>
    );
  }
}
