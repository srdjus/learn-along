import React from "react";
import {draw} from "canvas";

//Child of RoomSingle

export class Canvas extends React.Component {
  componentDidMount() {
    var canvas = document.getElementById("mainCanvas");
    draw(canvas, this.props.socket, this.props.roomId);
  }

  render() {
    return(
      <div className="canvas-parent">
        <table id="color-palette">
        
        </table>
        <select id="brush-size">
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="4">4</option>
          <option value="12">12</option>
          <option value="32">32</option>
          <option value="70">70</option>
        </select>
        <canvas id="mainCanvas">
          I'm sorry, your browser does 
          not support html canvas element.
        </canvas>
      </div>
    );
  }
}
