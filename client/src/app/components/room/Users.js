import React from "react"; 

// Child of Single
export class Users extends React.Component {
  constructor() {
    super(); 
    this.state = {
      users: [],
      requests: []
    }

    this.oneJoined = this.oneJoined.bind(this);
    this.oneLeft = this.oneLeft.bind(this);
    this.getUsers = this.getUsers.bind(this); 
    this.penRequested = this.penRequested.bind(this);
    this.requestDeclined = this.requestDeclined.bind(this);
    this.requestAccepted = this.requestAccepted.bind(this);
  }

  componentDidMount() {
    this.props.socket.on("one-joined", this.oneJoined);
    this.props.socket.on("one-left", this.oneLeft);
    this.props.socket.on("get-users", this.getUsers);
    this.props.socket.on("pen-requested", this.penRequested);
  }

  getUsers(users) {
    this.setState({
      users: users,
    });
  }

  oneJoined(username) {
    let u = this.state.users; 
    u.push(username); 
    this.setState({
      users: u
    });
  }

  oneLeft(info) {
    let u = this.state.users; 
    let i = u.indexOf(info.username);
    u.splice(i, 1);
    this.setState({
      users: u
    });

    if (info.isAdmin)
      // Set a new admin (first user in the user array)
      // This will be rewritten
      if (this.state.users[0] === this.props.username)
        this.props.socket.emit("trans-admin");
  }

  leaveRoom(e) {
    e.preventDefault();
    this.props.socket.emit("leave-room");
    this.props.verifyLog(false, null);
  }

  penRequested(username) {
    let r = this.state.requests || []; 
    r.push(username);
    this.setState({
      requests: r
    });
  }

  requestPen(e) {
    e.preventDefault(); 
    this.props.socket.emit("request-pen", this.props.username);
  }

  requestDeclined(req, e) {
    e.preventDefault(); 
    let r = this.state.requests; 
    r.splice(r.indexOf(req), 1); 
    this.setState({
      requests:r 
    });
  }

  requestAccepted(req, e) {
    e.preventDefault();
    this.props.socket.emit("new-admin", req);
    this.setState({
      requests: []
    });
  }

  render() {
    return(
      <div>
         <button className="btn btn-xs btn-danger"
                  onClick={this.leaveRoom.bind(this)} >
            Leave the room
          </button>
          <button className="btn btn-xs btn-warning"
                onClick={this.requestPen.bind(this)} >
          Request the pen
        </button>
        <h3>Users</h3>
          {
            this.state.users.map(user =>
              <a>{user} </a>
            )
          }
          <br />
         
          {
            this.state.requests.map(req => 
              <div className="alert alert-warning" role="alert">
                  <a href="#" onClick={this.requestAccepted.bind(this, req)}>Accept</a>/
                  <a href="#" onClick={this.requestDeclined.bind(this, req)}>Decline</a> {req} is requesting the brush
              </div>                            
            )
          }
      </div>
    );
  }
}


