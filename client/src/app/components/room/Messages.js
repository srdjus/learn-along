import React from "react";

// Child of Single
export class Messages extends React.Component {
  constructor() {
    super(); 
    this.state = {
      messages: [
        {
          message: "Welcome!",
          username: "Initial message"
      }]
    };

    this.emitMessage = this.emitMessage.bind(this);
  }

  componentDidMount() {
    this.props.socket.on("got-message", this.emitMessage);
  }

  sendMessage(e) {
    e.preventDefault();
    let msgInfo = {
      message: this.refs.myInput.value,
      roomId: this.props.id
    };
    this.props.socket.emit("message", msgInfo);
    this.refs.myInput.value = "";
  }

  emitMessage(res) {
    let msgs = this.state.messages;
    msgs.push(res);
    if (msgs.length > 7)
      msgs.shift();
    this.setState({ 
      messages: msgs 
    });
  }

  render() {
    return (
      <div>
        <h3>Chat Room</h3>
        {
          this.state.messages.map(message =>
            <div className="well well-sm">
              <strong>{message.username}: </strong>{message.message}
            </div>)
        }
        <h5>Send Message</h5>
          <form id="message" onSubmit={this.sendMessage.bind(this)}>
            <input className="form-control" 
                   type="text" 
                   ref="myInput" />
            <br />
            <button type="submit" 
                    form="message" 
                    className="btn btn-primary">
              Send
            </button>
          </form>
        <hr />
      </div>
    );
  }
}
