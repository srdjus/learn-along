import React from "react";

export class Join extends React.Component {
  constructor() {
    super();
    this.state = {
      failed: false
    }
    this.handleJoin = this.handleJoin.bind(this);
    this.handleReject = this.handleReject.bind(this);
  }
  
  componentDidMount() {
    this.refs.username.focus();
    this.props.socket.on("handle-join", this.handleJoin);
    this.props.socket.on("handle-reject", this.handleReject);
  }

  handlePassword(e) {
    e.preventDefault();
    this.props.socket.emit("check-password", {
      username: this.refs.username.value,
      key: this.refs.key.value,
      roomId: this.props.id
    });
  }

  handleJoin(username) {
  // This will be rewritten
    let info = {
      username: username,
      _id: this.props.id
    };

    this.props.socket.emit("join-room", info);
    this.setState({
      failed: false,
      msg: null
    });

    this.props.verifyLog(true, info.username);
  }

  handleReject(msg) {
    this.setState({
      failed: true,
      msg: msg
    });
  }

  render() {
    let Failed = "";
    if (this.state.failed) {
      if (this.state.msg === "key")
        Failed = (
        <div className="alert alert-danger">
          <strong>Wrong key</strong> Enter the correct key.
        </div>);

      else if (this.state.msg === "uniqueness")
        Failed = (
        <div className="alert alert-warning">
          <strong>Name taken</strong> Please choose different in order to join room.
        </div>);
    }
    return (
      <div>
        {Failed}
        <form id="join" onSubmit={this.handlePassword.bind(this)}>
          <div className="form-group">
            <label>Pick username:</label>
            <input className="form-control" type="text" ref="username" />
          </div>
          <div className="form-group">
            <label>Enter password you received from room creator/member:</label>
            <input className="form-control" type="password" ref="key" />
          </div>
          <br />
          <button type="submit" form="join" className="btn btn-primary">Enter</button>
        </form>
      </div>
    );
  }
}
