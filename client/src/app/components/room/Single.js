import React from "react";

// Components
import {Canvas} from "Canvas";
import {Messages} from "Messages";
import {Join} from "Join";
import {Users} from "Users";

// Child of Room
export class Single extends React.Component {
  constructor(props) {
    super();
    this.state = {
      logged: false,
      roomInfo: props.roomInfo
    };

    this.verifyLog = this.verifyLog.bind(this);
  }

  verifyLog(s, username) {
    this.setState({
      logged: s,
      username: username
    });
  }

  render() {
    let JoinRoom = (
      <Join
        verifyLog={this.verifyLog.bind(this)}
        socket={this.props.socket}
        id={this.state.roomInfo._id}
      />
    );
    
    let Chat = "";
    let CanvasArea = "";
    let UserList = "";

    if (this.state.logged) {
      JoinRoom = "";
      Chat = (
          <Messages
            messages={this.state.messages}
            socket={this.props.socket}
            id={this.state.roomInfo._id}
          />
      );

      CanvasArea = (
          <Canvas
            socket={this.props.socket}
            roomId={this.state.roomInfo._id}
          />
      );

      UserList = (
        <Users 
          verifyLog={this.verifyLog.bind(this)}
          username={this.state.username}
          socket={this.props.socket}
        />
      ); 
    }

    return (
      <div>
        <div className="container">
          <div className="row">
            <div className="col-xs-5">
              <h3>
                {this.state.roomInfo.name}
              </h3>
              <p>
                {this.state.roomInfo.description}
              </p>
            </div>
          </div>
          <div className="row">
            <div className="col-xs-6">
              {JoinRoom}
            </div>
          </div>
        </div>
        <hr />
        <div className="row">
          <div className="col-md-10">
            {CanvasArea}
          </div>
          <div className="col-md-2">
            {Chat}
          </div>
        </div>
        <div className="row">
          <div className="col-xs-5">
              {UserList}
          </div>
        </div>
      </div>
    );
  }
}
