import React from "react";
import {Single} from "Single";

// Child of Root
export class Room extends React.Component {
  constructor(props) {
    super();
    this.state = {
      exists: false,
      roomInfo: {}
    };
  }

  componentWillMount() {
    this.props.socket.emit("find-room", this.props.params.id);
    this.props.socket.on("room-found", (result) => {
        this.setState({
          exists: true,
          roomInfo: result
        });
    });
  }

  render() {
    let Content = this.state.exists ? 
      <Single 
        roomInfo={this.state.roomInfo} 
        socket={this.props.socket}/> 
      : " ";

    return (
      <div>
        {Content}
      </div>
    );
  }
}
