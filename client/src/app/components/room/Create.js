import React from "react";

// Child of RoomSingle

export class Create extends React.Component {
  constructor() {
    super();
    this.state = {
      created: false,
      roomId: "none",
      error: false
    };
  }

  handleSubmit(e) {
    e.preventDefault();
    let roomInfo = {
      name: this.refs.name.value,
      description: this.refs.description.value,
      key: this.refs.key.value,
      users: []
    };

    this.props.socket.emit("create-room", roomInfo);
  }

  tryAgain() {
    this.setState({
      error: false
    });
  }

  componentDidMount() {
    this.refs.name.focus(); 
    this.props.socket.on("room-created", (result) => {
      this.setState({
        created: true,
        error: false,
        roomId: result
      });
    });

    this.props.socket.on("error-creating", () => {
      this.setState({
        error: true
      });
    })
  }

  render() {
    let InfoMessage = "";
    let Errors = "";
    let CreateRoom = (
      <div>
        <h3>Create a room</h3>
        <div>
          <form id="create-room" onSubmit={this.handleSubmit.bind(this)}>
            <div className="form-group">
              <label>Room name:</label>
              <input className="form-control" type="text" ref="name" />
            </div>
            <div className="form-group">
              <label>Description:</label>
              <input className="form-control" type="text" ref="description" />
            </div>
            <div className="form-group">
              <label>Enter security key:</label>
              <input className="form-control" type="password" ref="key" />
            </div>
            <p><i>
              The name does not have to be unique.
              Choose whatever you like as long as it is
              shorter than 15 chars. Same goes for the description, 
              except it is limited to 30. (hyphens, underscores and spaces are allowed)
            </i></p>
            <p><i>
              The key length is limited to 10 chars (at the most).
            </i></p>
            <p><i>
              Also, you can omit the key field by leaving it empty.
            </i></p>
            <button type="submit" form="create-room" className="btn btn-primary">
              Create
            </button>
          </form>
        </div>
      </div>
    );
    if (this.state.error) {
      CreateRoom = "";
      Errors = (
        <div>
          <div className="alert alert-danger">
            <strong>Invalid input</strong> There were problems creating your room,
            please review following:
          </div>
          <p><i>
            Name should follow format: "Room Name"
          </i></p>
          <p><i>
            Description should follow format: "Room Description"
          </i></p>
          <p><i>
            Key length is limited to 10 characters. Also it is optional, 
            and you can leave it blank if you want so.
          </i></p>
          <button className="btn btn-warning" onClick={this.tryAgain.bind(this)}>
              Go back
          </button>
        </div>
      );
    }

    if (this.state.created) {
      CreateRoom = "";
      InfoMessage = (
        <div>
          <div className="alert alert-success">
            <strong>Great! </strong> You've created a room successfully!
          </div>
          <p>
            Visit your room by clicking the link below.
            You'll receive the sharing url after you enter the room.
          </p>
          <p><a href={"/room/" + this.state.roomId}>Open</a></p>
        </div>
      );
    }
    return (
      <div className="container">
        <div className="row">
          <div className="col-md-6 col-md-offset-3">
            <div className="jumbotron">
              {Errors}
              {InfoMessage}
              {CreateRoom}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
