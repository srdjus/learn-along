import React from "react"; 

export const Error = (props) => {
  return(
    <div className="col-xs-8 col-xs-offset-2">
      <div className="alert alert-danger" role="alert">
        <strong>{props.status}</strong> Looks like something went wrong.
      </div>
    </div>
  );
}