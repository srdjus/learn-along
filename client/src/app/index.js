import React from "react";
import {render} from "react-dom";
import {Router, Route, browserHistory, IndexRoute} from "react-router";


import {Root} from "Root";
import {Create} from "Create";
import {Room} from "Room";
import {About} from "About";

class App extends React.Component {
  render() {
    return(
      <Router history={browserHistory}>
        <Route path={"/"} component={Root} >
          <IndexRoute component={Create} />
          <Route path={"create"} component={Create} />
          <Route path={"room/:id"} component={Room} />
          <Route path={"about"} component={About} />
        </Route>
      </Router>
    );
  }
}

render(<App />, window.document.getElementById("app"));
