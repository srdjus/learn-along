var path = require("path");

var DIST_DIR = path.resolve(__dirname, "client/dist");
var SRC_DIR = path.resolve(__dirname, "client/src");

module.exports = {
  entry: SRC_DIR + "/app/index.js",
  output: {
    path: DIST_DIR + "/app",
    filename: "bundle.js",
    publicPath: "/dist/app/"
  },
  resolve: {
    modules: [
      "node_modules", 
      path.resolve(SRC_DIR, "app"), 
      path.resolve(SRC_DIR, "app/helpers"), 
      path.resolve(SRC_DIR, "app/components"), 
      path.resolve(SRC_DIR, "app/components/room")
    ]
  },
  module: {
    loaders: [
      {
        test: /\.js?/,
        include: SRC_DIR,
        loader: "babel-loader",
        query: {
          presets: ["react", "es2015", "stage-2"]
        }
      }
    ]
  }
};
