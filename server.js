var path = require("path");
var assert = require("assert");
var express = require("express");
var http = require("http");
var app = express();
var server = http.createServer(app);
var io = require("socket.io").listen(server);

// Models
var Room = require('./server/models/Room');

app.use(express.static(path.resolve(__dirname, "client")));

io.on("connection", function(socket) {
  socket.on("message", function(info) {
    io.sockets.in(info.roomId)
      .emit("got-message", {
      username: socket.username,
      message: info.message
    });
  });

  socket.on("join-room", function(info) {
    // If socket is already associated with the room 
    if (socket.roomId) 
      socket.emit("already-in");
    
    Room.joinRoom(info._id, info.username, function(err) {
      if (err)
        socket.emit("status", 500);
      else 
        Room.getUsers(info._id, function(err, users) {
          if (err)
            socket.emit("status", 500);
          else {
            socket.username = info.username;
            socket.roomId = info._id;
            socket.join(info._id);

            // Send a list of users who are already in to the new user
            socket.emit("get-users", users);
            
            // Notify everyone in room
            socket.broadcast.to(info._id)
              .emit("one-joined", info.username);
            
            if (io.sockets.adapter.rooms[info._id].length === 1) {
              socket.admin = true;
              socket.emit("admin", true); 
            }
          }
        }); 
    });
  });

  socket.on("disconnect", function() {
    /* If socket.roomId is false it means that
    user didn't join any room */
    if (socket.roomId)
      Room.leaveRoom(socket.roomId, socket.username, function(err) {
        if (err)
          socket.emit("status", 500); 
        else 
          io.sockets.in(socket.roomId)
            .emit("one-left", { 
              username: socket.username,
              isAdmin: socket.admin 
            });
      });
  }); 
  
  socket.on("leave-room", function() {
    Room.leaveRoom(socket.roomId, socket.username, function(err) {
      if (err)
        socket.emit("status", 500); 
      else 
        socket.disconnect();
    });
  });
  
  socket.on("check-password", function(creds) {
    Room.checkPassword(creds, function(err, answer) {
      if (err)
        if (err.message === "key")
          socket.emit("handle-reject", err.message);
        else if (err.message === "uniqueness")
          socket.emit("handle-reject", err.message)
        else
          socket.emit("server-error");
      else if (answer)
        socket.emit("handle-join", answer);
    });
  });

  socket.on("create-room", function(room) {
    Room.createRoom(room, function(err, room) {
      if (err) 
        if (err.message === "validation")
          socket.emit("error-creating");
        else
          socket.emit("status", 500); 
      else 
        socket.emit("room-created", room.insertedId);
    });
  });

  socket.on("trans-admin", function() {
    socket.admin = true;
    socket.emit("admin", true);
  });

  socket.on("new-admin", function(username) {
    var r = io.sockets.adapter.rooms[socket.roomId]; 
    if (r) {
      Object.keys(r.sockets).forEach(function(item) {
        var s = io.sockets.connected[item];
        if (s.username === username)
          s.emit("admin", s.admin = true);
        
        socket.emit("admin", socket.admin = false);
      });
    }
  });
  
  socket.on("request-pen", function(username) {
    var r = io.sockets.adapter.rooms[socket.roomId]; 
      if (r) {
        Object.keys(r.sockets).forEach(function(item) {
          var s = io.sockets.connected[item];
          if (s.admin) 
            s.emit("pen-requested", username);
        });
      }
  });

  socket.on("find-room", function(roomId) {
    Room.findOne(roomId, function(err, result) {
      if (err) 
        if (err.message === "Invalid ID")
          socket.emit("status", 404);
        else 
          socket.emit("status", 500);
      
      else if (!result)
        socket.emit("status", 404);     
      else 
        socket.emit("room-found", result);
    });
  });

  socket.on("point", function(info) {
    if (socket.admin)
      io.to(info.roomId).emit("point", info);
  });

  socket.on("mouseup", function(info) {
    if (socket.admin)
      io.to(info.roomId).emit("mouseup");
  });

  socket.on("brush-color", function(info) {
    if (socket.admin)
      io.to(info.roomId).emit("color-changed", info.color);
  });

  socket.on("brush-size", function(info) {
    if (socket.admin)
      io.to(info.roomId).emit("size-changed", info.size);
  });
});

app.get('*', function(req, res) {
  res.sendFile(path.resolve(__dirname, "client/index.html"));
});

app.set("port", process.env.PORT || 5000);

server.listen(app.get('port'), function(err) {
  assert.equal(err, null);
  console.log("Successfully listening to:", app.get('port'));
});
