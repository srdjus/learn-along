var assert = require("assert");
var dotenv = require('dotenv').load();

// Mongo
var mongoClient = require("mongodb").MongoClient;
var ObjectId = require("mongodb").ObjectID;
var url = process.env.PROD_MONGODB;

var db = null;
mongoClient.connect(url, function(err, database) {
  assert.equal(err, null);
  db = database;
});


module.exports = {
  checkPassword: function(creds, callback) {
    validateId(creds.roomId, function(err) {
      if (err)
        callback(err);
      else
        db.collection("rooms").findOne({
          _id: ObjectId(creds.roomId)
          })
          .then(function(result) {
            if (creds.key != result.key)
              callback(new Error("key"));
            else if (result.users.indexOf(creds.username) >= 0)
              callback(new Error("uniqueness"));
            else 
              callback(null, creds.username);
          })
          .catch(function(err) {
            callback(err);
          });
    });
  },

  createRoom: function(room, callback) {
    validate(room, function(err, room) {
      if (err) 
        callback(err);  
      else
        db.collection("rooms")
          .insertOne(room)
          .then(room => {
            callback(null, room);
          })
          .catch(err => {
            callback(err);
          });
    });
  },

  findOne: function(id, callback) {
    validateId(id, function(err, id) {
      if (err)
        callback(err);
      else
        db.collection("rooms").findOne(
          {
            _id: ObjectId(id)
          },
          {
            name: 1, 
            description: 1,
            users: 1
          })
          .then((result) => {
            callback(null, result);
          })
          .catch(err => {
            callback(err);
          });
    });
  }, 
  
  joinRoom: function(id, username, callback) {
    validateId(id, function(err, validId) {
      if (err)
        callback(err); 
      else 
        db.collection("rooms").update(
          { 
            _id: ObjectId(validId) 
          }, 
          {
            $push: {
              users: username
            },
          })
          .then(() => {
            callback(null);
          })
          .catch(err => {
            callback(err);
          });      
    });
  }, 

  leaveRoom: function(id, username, callback) {
    validateId(id, function(err, validId) {
      if (err)
        callback(err);
      else 
        db.collection("rooms").update(
          {
            _id: ObjectId(validId)
          }, 
          {
            $pull: {
              users: username
            }
          })
          .then(() => {
            callback(err);
          })
          .catch(err => {
            callback(err);
          });
    });
  },

  getUsers: function(id, callback) {
    validateId(id, function(err, validId) {
      if (err)
        callbacke(err);
      else 
        db.collection("rooms").findOne(
          {
            _id: ObjectId(validId)
          }, 
          {
            users: 1
          })
          .then((result) => {
            callback(null, result.users);
          })
          .catch(err => {
            callback(err);
          });
    })
  }
  
}

// Helper functions
function validateId(id, callback) {
  if (ObjectId.isValid(id))
    callback(null, id);
  else
    callback(new Error("Invalid ID"));
};

function validate(r, callback) {
  /* Probably it is better to also pass a 
  fully descripted error message but for know
  I will just do it simply */

  var pat = /^[-\w\s]+$/;

  // This if-if-if format looks awful, 
  // but in this case is much more readable
  if (!pat.test(r.name) || r.name.length > 15) 
    callback(new Error("validation"));
  
  else if (!pat.test(r.description) || r.description.length > 30) 
    callback(new Error("validation"));
  
  else if (r.key.length > 10) 
    callback(new Error("validation"));
  
  else 
    callback(null, r);
};
